package com.xhlim.demo.mybatis.paas.config;

import com.shtel.paas.sdk.core.mybatis.EnablePaasMybatis;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 使用paas-sdk时必须加入注解 @EnableAutoConfiguration 否则H2数据库配置不解析
 *
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-28
 */
@Configuration
@EnablePaasMybatis
@MapperScan({"com.xhlim.demo.mybatis.paas.mapper"})
@EnableAutoConfiguration
public class MybatisConfig {
}
