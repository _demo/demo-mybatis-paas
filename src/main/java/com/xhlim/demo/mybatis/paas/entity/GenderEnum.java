package com.xhlim.demo.mybatis.paas.entity;

/**
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-23
 */
public enum GenderEnum {

    // 未知、男性、女性
    UNKNOW, MALE, FEMALE;
}
