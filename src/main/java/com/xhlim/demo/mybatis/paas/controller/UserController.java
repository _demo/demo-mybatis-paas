package com.xhlim.demo.mybatis.paas.controller;

import com.xhlim.demo.mybatis.paas.entity.User;
import com.xhlim.demo.mybatis.paas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-23
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/api/v1/users")
    public List<User> users() {
        return userService.findAll();
    }

}
