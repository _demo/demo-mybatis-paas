package com.xhlim.demo.mybatis.paas.mapper;

import com.shtel.paas.sdk.core.mybatis.common.PaasBaseMapper;
import com.xhlim.demo.mybatis.paas.entity.User;

import java.util.List;

/**
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-26
 */
public interface UserMapper extends PaasBaseMapper<User> {

    /**
     * 保存
     *
     * @return
     */
    int save(User user);
    /**
     * 查询所有用户
     *
     * @return
     */
    List<User> findAll();

}
