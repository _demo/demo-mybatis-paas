package com.xhlim.demo.mybatis.paas.service;

import com.xhlim.demo.mybatis.paas.entity.User;

import java.util.List;

/**
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-23
 */
public interface UserService {

    List<User> findAll();
}
