package com.xhlim.demo.mybatis.paas.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-23
 */
@Getter
@Setter
@ToString
@Accessors(chain = true)
@Table(name = "t_user")
public class User implements Serializable {

    private Long id;
    /**
     * 用户名称
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private Integer sex;

}
