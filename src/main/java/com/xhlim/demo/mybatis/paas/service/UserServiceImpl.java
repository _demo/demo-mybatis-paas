package com.xhlim.demo.mybatis.paas.service;

import com.xhlim.demo.mybatis.paas.mapper.UserMapper;
import com.xhlim.demo.mybatis.paas.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author xhlim@outlook.com
 * @version 1.0
 * @date 2019-08-23
 */
@Service
public class UserServiceImpl implements UserService {

    private static Random RANDOM = new Random();

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        userMapper.insert(new User().setName("install-" + UUID.randomUUID().toString()).setAge(RANDOM.nextInt(100))
                                    .setSex(RANDOM.nextInt(3)));

        userMapper.save(new User().setName("save-" + UUID.randomUUID().toString()).setAge(RANDOM.nextInt(100))
                                  .setSex(RANDOM.nextInt(3)));
        List<User> list = userMapper.findAll();
        list.forEach(System.out::println);
        return list;
    }
}
