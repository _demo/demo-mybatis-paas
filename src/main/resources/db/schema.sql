CREATE TABLE IF NOT EXISTS `t_user`
(
    `id`   int(32)     NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` varchar(64) NULL COMMENT '姓名',
    `age`  int(1)      NULL COMMENT '年龄',
    `sex`  int(1)      NULL COMMENT '性别【0-未知,1-男,2-女】',
    PRIMARY KEY (`id`)
) COMMENT = '用户信息';
