# demo-mybatis-paas

基于理想PaaS-SDK搭建的mybatis案例

## 启动方式

### 启动类

`com.shtel.paas.sdk.app.PaasMainApp`

### 启动配置（Program arguments）

```
--shtelpaas.app.name=mybatis
--shtelpaas.app.basepkg=com.xhlim.demo
--shtelpaas.app.config.profile=app$dev$0.0.1
--shtelpaas.log.profile=log$dev$0.0.1
--shtelpaas.app.nameserver=http://x.x.x.x/eureka/
--shtelpaas.app.config.mode=local

```

> `shtelpaas.app.name` 服务名称  
> `shtelpaas.app.basepkg` 项目根目录  
> `shtelpaas.app.config.profile` 配置文件版本  
> `shtelpaas.log.profile` 日志文件版本  
> `shtelpaas.app.nameserver` 注册中心地址  
> `shtelpaas.app.config.mode` 读取本地配置 
